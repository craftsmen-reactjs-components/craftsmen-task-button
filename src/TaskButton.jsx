import React, { useState } from "react";
import { Button, Spinner } from "reactstrap";

function TaskButton({
  onExecute, // (taskCompleteCallback(nextTaskData = undefined), taskData)
  onClick, // (e, taskCompleteCallback(nextTaskData = undefined), taskData)
  executingText, // text to display when button is clicked
  initialTaskData, // initial data for taskData that will be passed to onExecute callback
  disabled,
  children,
  ...buttonProps
}) {
  executingText = executingText || children;

  const [executing, setExecute] = useState(false);
  const [taskData, setTaskData] = useState(initialTaskData);

  const taskCompleteCallback = (nextTaskData = undefined) => {
    if (nextTaskData !== undefined) {
      setTaskData(nextTaskData);
    }
    setExecute(false);
  };

  const buttonClickHandler = (e) => {
    setExecute(true);

    if (onExecute) {
      onExecute(taskCompleteCallback, taskData);
    }

    if (onClick) {
      onClick(e, taskCompleteCallback, taskData);
    }
  };

  let buttonChildren = null;
  if (executing) {
    console.log("Executing text: ", executingText);
    buttonChildren = (
      <span>
        <Spinner color="light" size="sm" /> {executingText}
      </span>
    );
  } else {
    buttonChildren = children;
  }

  return (
    <Button
      {...buttonProps}
      disabled={executing || disabled}
      onClick={buttonClickHandler}
    >
      {buttonChildren}
    </Button>
  );
}

export default TaskButton;
