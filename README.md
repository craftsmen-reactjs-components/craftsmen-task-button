# craftsmen-task-button

> Task button ReactJS component
> Component based on Reactstrap Button component with additional features

[![NPM](https://img.shields.io/npm/v/craftsmen-task-button.svg)](https://www.npmjs.com/package/craftsmen-task-button) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save craftsmen-task-button
```

## Available features

- Button based on Reactstrap Button component
- Included bootstrap styling
- Included loading animation with spinner
- Customizable text that can be applied to button 
- Task complete callback that allows you to chain your next action after button click

## Simple Usage

```jsx
import React from "react"

import TaskButton from "craftsmen-task-button"

const Example = () => {
  return (
    <TaskButton
      executingText="Never ending loading..."
    >
      Click me!
    </TaskButton>
  )
}
```

## Delayed Loading

```jsx
import React from "react"

import TaskButton from "craftsmen-task-button"

const Example = () => {
  return (
    <TaskButton 
        executingText="5 seconds loading..." 
        onExecute={(taskCompleteCallback) => {
          setTimeout(taskCompleteCallback, 5000)
        }}
      >
        Load!
    </TaskButton>
  )
}
```
## Performing specific task example

```jsx
import React from "react"

import TaskButton from "craftsmen-task-button"

const Example = () => {
  return (
    <TaskButton 
        executingText="Handling your task..." 
        initialTaskData={{taskId: 1, message: "Custom task data object"}}
        onExecute={(taskCompleteCallback, taskData) => {
          console.log("Button executing the following task: ", taskData)
            setTimeout(() => {
              // let's handle taskData and fetch something...
              let newTaskData = {...taskData}
              newTaskData.taskId++;
              newTaskData.message = "Timeout handled previous task!"
              taskCompleteCallback(newTaskData);
            }, 3000)
          }}
        >
      Execute Task!
    </TaskButton>
  )
}
```

## Note

- For more details of the implmentation, please check **example** project in this repository

## Author

- SaaS Craftsmen Team

## License

 © [](https://github.com/)
