import React, { useState } from 'react';
import { Button, Spinner } from 'reactstrap';

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function TaskButton(_ref) {
  var onExecute = _ref.onExecute,
      onClick = _ref.onClick,
      executingText = _ref.executingText,
      initialTaskData = _ref.initialTaskData,
      disabled = _ref.disabled,
      children = _ref.children,
      buttonProps = _objectWithoutPropertiesLoose(_ref, ["onExecute", "onClick", "executingText", "initialTaskData", "disabled", "children"]);

  executingText = executingText || children;

  var _useState = useState(false),
      executing = _useState[0],
      setExecute = _useState[1];

  var _useState2 = useState(initialTaskData),
      taskData = _useState2[0],
      setTaskData = _useState2[1];

  var taskCompleteCallback = function taskCompleteCallback(nextTaskData) {
    if (nextTaskData === void 0) {
      nextTaskData = undefined;
    }

    if (nextTaskData !== undefined) {
      setTaskData(nextTaskData);
    }

    setExecute(false);
  };

  var buttonClickHandler = function buttonClickHandler(e) {
    setExecute(true);

    if (onExecute) {
      onExecute(taskCompleteCallback, taskData);
    }

    if (onClick) {
      onClick(e, taskCompleteCallback, taskData);
    }
  };

  var buttonChildren = null;

  if (executing) {
    console.log("Executing text: ", executingText);
    buttonChildren = /*#__PURE__*/React.createElement("span", null, /*#__PURE__*/React.createElement(Spinner, {
      color: "light",
      size: "sm"
    }), " ", executingText);
  } else {
    buttonChildren = children;
  }

  return /*#__PURE__*/React.createElement(Button, _extends({}, buttonProps, {
    disabled: executing || disabled,
    onClick: buttonClickHandler
  }), buttonChildren);
}

export default TaskButton;
//# sourceMappingURL=index.modern.js.map
