import React from 'react'

import TaskButton from 'craftsmen-task-button'
import 'bootstrap/dist/css/bootstrap.css';

const App = () => {
  return <>
      <TaskButton
        executingText="Never ending loading..." 
      >
        Click me!
      </TaskButton> 

      <br/> <br/>

      <TaskButton 
        executingText="5 seconds loading..." 
        onExecute={(taskCompleteCallback) => {
          setTimeout(taskCompleteCallback, 5000)
        }}
      >
        Load!
      </TaskButton>

      <br/> <br/>

      <TaskButton 
        executingText="5 seconds loading..."
        onClick={(e, taskCompleteCallback) => {
          setTimeout(taskCompleteCallback, 5000)
        }}
      >
        Load! with onClick
      </TaskButton>

      <br/> <br/>

      <TaskButton 
        executingText="Handling your task..." 
        initialTaskData={{taskId: 1, message: "Custom task data object"}}
        onExecute={(taskCompleteCallback, taskData) => {
          console.log("Button executing the following task: ", taskData)
          setTimeout(() => {
            // lets handle taskData and fetch something...
            let newTaskData = {...taskData}
            newTaskData.taskId++;
            newTaskData.message = "Timeout handled previous task!"
            taskCompleteCallback(newTaskData);
          }, 3000)
        }}
      >
        Execute Task!
      </TaskButton>


      
    </>
}



export default App
